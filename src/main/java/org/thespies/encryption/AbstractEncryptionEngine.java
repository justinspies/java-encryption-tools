package org.thespies.encryption;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * 
 * @author justin.spies <justin@thespies.org>
 * 
 */
public abstract class AbstractEncryptionEngine {
	/**
	 * Default character set used when encrypting data.
	 */
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private static final Logger LOGGER = Logger.getLogger(AbstractEncryptionEngine.class);

	private Charset charset = Charset.forName(DEFAULT_CHARSET_NAME);

	private String cipher;

	private String hashType;

	private String secretKeyAlgorithm;

	private String encryptionAlgorithm;

	private int saltLength;

	/**
	 * Returns the algorithm used for hashing encrypted data in order to
	 * guarantee message security.
	 * 
	 * @return the hashType
	 */
	public String getHashType() {
		return hashType;
	}

	/**
	 * Set the algorithm used for hashing encrypted data in order to guarantee
	 * message security.
	 * 
	 * @param hashType
	 *            the hashType to set
	 */
	public void setHashType(String hashType) {
		this.hashType = hashType;
	}

	/**
	 * Returns the algorithm used for creating an encrypted secret key from
	 * plain text.
	 * 
	 * @return the secretKeyAlgorithm
	 */
	public String getSecretKeyAlgorithm() {
		return secretKeyAlgorithm;
	}

	/**
	 * Sets the algorithm used for creating an encrypted secret key from plain
	 * text.
	 * 
	 * @param secretKeyAlgorithm
	 *            the secretKeyAlgorithm to set
	 */
	public void setSecretKeyAlgorithm(String secretKeyAlgorithm) {
		this.secretKeyAlgorithm = secretKeyAlgorithm;
	}

	/**
	 * @return the encryptionAlgorithm
	 */
	public String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}

	/**
	 * @return the saltLength
	 */
	public int getSaltLength() {
		return saltLength;
	}

	/**
	 * @param saltLength
	 *            the saltLength to set
	 */
	public void setSaltLength(int saltLength) {
		if (this.isSaltLengthValid(saltLength)) {
			this.saltLength = saltLength;
		} else {
			throw new InvalidSaltLengthException("The salt length specified " + saltLength + " is not valid");
		}
	}

	/**
	 * Determines if the specified salt has a length that is valid for a
	 * concrete encryption engine implementation.
	 * 
	 * @return true if the length of the salt string is valid.
	 */
	public abstract boolean isSaltLengthValid(int saltLength);

	/**
	 * @return the charset
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * @param charset
	 *            the charset to set
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * @return the cipher
	 */
	public String getCipher() {
		return cipher;
	}

	/**
	 * @param cipher
	 *            the cipher to set
	 */
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}

	/**
	 * Generates a message hash for the specified text, returning the hash as a
	 * {@link byte} array that is typically Base64 encoded.
	 * 
	 * @param what
	 *            the message for which the hash is generated
	 * @return a {@link byte} array of the
	 */
	public byte[] hash(final String what) {
		// Create and return the hash.
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Calculating hash for using " + this.getHashType() + " and key " + this.getHashType());
		}
		final MessageDigest digest;
		final byte[] hash;

		try {
			digest = MessageDigest.getInstance(this.getHashType());
			hash = digest.digest(what.getBytes(Charset.forName(DEFAULT_CHARSET_NAME)));
			return hash;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("Could not generate hash - the algorithm " + this.getHashType() + " could not be found", e);
		}

		return new byte[0];
	}

	/**
	 * Generate a random salt value using the {@link SecureRandom class}. The
	 * salt is initially a {@link byte} array that is then {@link Base64}
	 * encoded before being returned.
	 * 
	 * @return a string value of a randomly generated number value to be used as
	 *         the salt
	 */
	public String generateRandomSalt() {
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("generateSalt - START");
		}

		final SecureRandom random = new SecureRandom();
		final byte[] data = new byte[this.getSaltLength()];

		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Generating random number for use as the SALT");
		}
		random.nextBytes(data);

		final String retval = Base64.encodeBase64String(data);

		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("generateSalt - FINISH");
		}
		return retval;
	}

	/**
	 * 
	 * @param salt
	 * @param passphrase
	 * @return
	 */
	public abstract SecretKeySpec getKey(final String salt, final String passphrase);

	/**
	 * Encrypt the data specified by <i>what</i> using the specified key. Will
	 * return null if any exceptions are encountered during the encryption
	 * process.
	 * 
	 * @param what
	 *            the data to be encrypted
	 * @param key
	 *            the {@link SecretKey} used to perform the encryption
	 * @return a {@link byte} array of the encrypted bytes; typically this value
	 *         will later be base64 encoded if a string representation is
	 *         required
	 */
	public abstract byte[] encrypt(String what, SecretKey key);

	/**
	 * Decrypt the data specified by <i>what</i> using the specified key. Will
	 * return null if any exceptions are encountered during the decryption
	 * process.
	 * 
	 * @param what
	 *            the data to be decrypted
	 * @param key
	 *            the {@link SecretKey} used to perform the encryption
	 * @return a String value representing the decrypted data
	 */
	public abstract String decrypt(final byte[] what, final SecretKeySpec key);
}
