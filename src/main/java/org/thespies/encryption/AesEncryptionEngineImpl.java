package org.thespies.encryption;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * @author justin.spies <justin@thespies.org>
 * 
 */
public class AesEncryptionEngineImpl extends AbstractEncryptionEngine {
	/**
	 * 
	 */
	public static final String DEFAULT_CIPHER = "AES/ECB/PKCS5Padding";

	/**
	 * 
	 */
	public static final String DEFAULT_HASH_TYPE = "SHA-512";

	/**
	 * 
	 */
	public static final String DEFAULT_ENCRYPTION_ALGORITHM = "PBKDF2WithHmacSHA1";

	/**
	 * 
	 */
	public static final String DEFAULT_SECRET_KEY_ALGORITHM = "AES";

	/**
	 * 
	 */
	public static final int DEFAULT_SALT_LENGTH = 50;

	/**
	 * 
	 */
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private static final Logger LOGGER = Logger.getLogger(AbstractEncryptionEngine.class);

	private Charset charset = Charset.forName(DEFAULT_CHARSET_NAME);

	private String cipher = DEFAULT_CIPHER;

	private String hashType = DEFAULT_HASH_TYPE;

	private String secretKeyAlgorithm = DEFAULT_SECRET_KEY_ALGORITHM;

	private String encryptionAlgorithm = DEFAULT_ENCRYPTION_ALGORITHM;

	private int saltLength = DEFAULT_SALT_LENGTH;

	/**
	 * @return the hashType
	 */
	public String getHashType() {
		return hashType;
	}

	/**
	 * @param hashType
	 *            the hashType to set
	 */
	public void setHashType(String hashType) {
		this.hashType = hashType;
	}

	/**
	 * @return the secretKeyAlgorithm
	 */
	public String getSecretKeyAlgorithm() {
		return secretKeyAlgorithm;
	}

	/**
	 * @param secretKeyAlgorithm
	 *            the secretKeyAlgorithm to set
	 */
	public void setSecretKeyAlgorithm(String secretKeyAlgorithm) {
		this.secretKeyAlgorithm = secretKeyAlgorithm;
	}

	/**
	 * @return the encryptionAlgorithm
	 */
	public String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}

	/**
	 * @param encryptionAlgorithm
	 *            the encryptionAlgorithm to set
	 */
	public void setEncryptionAlgorithm(String encryptionAlgorithm) {
		this.encryptionAlgorithm = encryptionAlgorithm;
	}

	/**
	 * @return the saltLength
	 */
	public int getSaltLength() {
		return saltLength;
	}

	/**
	 * @param saltLength
	 *            the saltLength to set
	 */
	public void setSaltLength(int saltLength) {
		this.saltLength = saltLength;
	}

	/**
	 * @return the charset
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * @param charset
	 *            the charset to set
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * @return the cipher
	 */
	public String getCipher() {
		return cipher;
	}

	/**
	 * @param cipher
	 *            the cipher to set
	 */
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}

	/**
	 * 
	 * @param what
	 * @return
	 */
	public byte[] hash(final String what) {
		// Create and return the hash.
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Calculating hash for using " + this.getHashType() + " and key " + this.getHashType());
		}
		final MessageDigest digest;
		final byte[] hash;

		try {
			digest = MessageDigest.getInstance(this.getHashType());
			hash = digest.digest(what.getBytes(Charset.forName(DEFAULT_CHARSET_NAME)));
			return hash;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("Could not generate hash - the algorithm " + this.getHashType() + " could not be found", e);
		}

		return new byte[0];
	}

	/**
	 * 
	 * @return
	 */
	public String generateSalt() {
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("generateSalt - START");
		}

		final SecureRandom random = new SecureRandom();
		final byte[] data = new byte[this.getSaltLength()];

		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Generating random number for use as the SALT");
		}
		random.nextBytes(data);

		final String retval = Base64.encodeBase64String(data);

		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("generateSalt - FINISH");
		}
		return retval;
	}

	/**
	 * 
	 * @param salt
	 * @param passphrase
	 * @return
	 */
	public SecretKeySpec getKey(final String salt, final String passphrase) {
		final byte[] localSalt = salt.getBytes(Charset.forName(DEFAULT_CHARSET_NAME));
		final int iterations = 10000;

		try {
			// Can also use 'AES' see
			// http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SecretKeyFactory
			final SecretKeyFactory factory = SecretKeyFactory.getInstance(this.getSecretKeyAlgorithm());
			final SecretKey tmp = factory.generateSecret(new PBEKeySpec(passphrase.toCharArray(), localSalt,
					iterations, 128));
			return new SecretKeySpec(tmp.getEncoded(), this.getEncryptionAlgorithm());

		} catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
			LOGGER.error("Could not generate encryption key, the required algorithm is not available", ex);
			return null;
		}
	}

	/**
	 * Encrypt the data specified by <i>what</i> using the specified key. Will
	 * return null if any exceptions are encountered during the encryption
	 * process.
	 * 
	 * @param what
	 *            the data to be encrypted
	 * @param key
	 *            the {@link SecretKey} used to perform the encryption
	 * @return a {@link byte} array of the encrypted bytes; typically this
	 *         should be base64 encoded if a string representation is required
	 */
	public byte[] encrypt(String what, SecretKey key) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Initiating encryption");
		}
		Cipher aes = null;
		String cipherType = DEFAULT_CIPHER;
		byte[] retval = new byte[0];

		try {
			aes = Cipher.getInstance(cipherType);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			LOGGER.error("Could not retrieve the Cipher instance for " + cipherType, e);
			return retval;
		}

		try {
			aes.init(Cipher.ENCRYPT_MODE, key);
		} catch (InvalidKeyException e) {
			LOGGER.error("The key specified for the encryption using " + cipherType + " is not valid", e);
			return retval;
		}

		try {
			retval = aes.doFinal(what.getBytes(Charset.forName(DEFAULT_CHARSET_NAME)));
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			LOGGER.error("Encryption of the data using " + cipherType + " failed", e);
		}

		return retval;
	}

	/**
	 * 
	 * @param what
	 * @param key
	 * @return
	 */
	public String decrypt(final byte[] what, final SecretKeySpec key) {
		LOGGER.debug("decrypt - Initiating decryption");
		String retval = null;

		try {
			final Cipher aes = Cipher.getInstance(DEFAULT_CIPHER);
			aes.init(Cipher.DECRYPT_MODE, key);

			final byte[] decrypted = aes.doFinal(what);
			retval = new String(decrypted, Charset.forName(DEFAULT_CHARSET_NAME));

		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			LOGGER.error("The select algorithm or padding could not be found", e);
		} catch (InvalidKeyException e) {
			LOGGER.error("The provided key is not valid", e);
		} catch (IllegalBlockSizeException e) {
			LOGGER.error("The selected block size is not valid", e);
		} catch (BadPaddingException e) {
			LOGGER.error("The message padding is bad", e);
		}

		return retval;
	}

	@Override
	public boolean isSaltLengthValid(int saltLength) {
		// TODO Auto-generated method stub
		return false;
	}
}
