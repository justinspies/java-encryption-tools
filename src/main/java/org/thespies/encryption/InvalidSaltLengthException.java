/**
 * 
 */
package org.thespies.encryption;

/**
 * @author justin.spies <justin@thespies.org>
 * 
 */
public class InvalidSaltLengthException extends RuntimeException {

	private static final long serialVersionUID = -8325224098587901598L;

	public InvalidSaltLengthException() {
		super();
	}

	public InvalidSaltLengthException(String message) {
		super(message);
	}

	public InvalidSaltLengthException(String message, Throwable cause) {
		super(message, cause);
	}

}
