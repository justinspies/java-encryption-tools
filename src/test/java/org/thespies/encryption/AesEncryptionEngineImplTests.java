package org.thespies.encryption;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author justin.spies <justin@thespies.org>
 *
 */
public class AesEncryptionEngineImplTests {
	private static final Logger LOGGER = Logger.getLogger(AesEncryptionEngineImplTests.class);

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#getHashType()}.
	 */
	@Test
	public void testGetHashType() {
		AesEncryptionEngineImpl engine = new AesEncryptionEngineImpl();
		assertEquals(AesEncryptionEngineImpl.DEFAULT_HASH_TYPE, engine.getHashType());
	
		Field field = null;
		
		try {
			field = engine.getClass().getDeclaredField("hashType");
		} catch (NoSuchFieldException | SecurityException e) {
			LOGGER.error("The hashType field has gone missing", e);
			fail("the hashType field has gone missing");
		}
		
		field.setAccessible(true);
		try {
			field.set(engine, "TEST");
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("The hashType field  could not be set", e);
			fail("the hashType field could not be set");
		}
		assertEquals("TEST", engine.getHashType());
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#setHashType(java.lang.String)}.
	 */
	@Test
	public void testSetHashType() {
		AesEncryptionEngineImpl engine = new AesEncryptionEngineImpl();

		engine.setHashType("TEST");
		
		Field field = null;
		
		try {
			field = engine.getClass().getDeclaredField("hashType");
		} catch (NoSuchFieldException | SecurityException e) {
			LOGGER.error("The hashType field has gone missing", e);
			fail("the hashType field has gone missing");
		}
		
		field.setAccessible(true);
		try {
			assertEquals("TEST", (String)field.get(engine));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("The hashType field  could not be set", e);
			fail("the hashType field could not be set");
		}
		assertEquals("TEST", engine.getHashType());
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#getSecretKeyAlgorithm()}.
	 */
	@Test
	public void testGetSecretKeyAlgorithm() {
		AesEncryptionEngineImpl engine = new AesEncryptionEngineImpl();
		assertEquals(AesEncryptionEngineImpl.DEFAULT_SECRET_KEY_ALGORITHM, engine.getSecretKeyAlgorithm());
		
		Field field = null;
		
		try {
			field = engine.getClass().getDeclaredField("secretKeyAlgorithm");
		} catch (NoSuchFieldException | SecurityException e) {
			LOGGER.error("The hashType field has gone missing", e);
			fail("the hashType field has gone missing");
		}
		
		field.setAccessible(true);
		try {
			field.set(engine, "TEST");
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("The hashType field  could not be set", e);
			fail("the hashType field could not be set");
		}
		assertEquals("TEST", engine.getSecretKeyAlgorithm());
		
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#setSecretKeyAlgorithm(java.lang.String)}.
	 */
	@Test
	public void testSetSecretKeyAlgorithm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#getEncryptionAlgorithm()}.
	 */
	@Test
	public void testGetEncryptionAlgorithm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#setEncryptionAlgorithm(java.lang.String)}.
	 */
	@Test
	public void testSetEncryptionAlgorithm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#getSaltLength()}.
	 */
	@Test
	public void testGetSaltLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#setSaltLength(int)}.
	 */
	@Test
	public void testSetSaltLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#hash(java.lang.String)}.
	 */
	@Test
	public void testHash() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#generateSalt()}.
	 */
	@Test
	public void testGenerateSalt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#getKey(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testGetKey() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#encrypt(java.lang.String, javax.crypto.SecretKey)}.
	 */
	@Test
	public void testEncrypt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.thespies.encryption.AbstractEncryptionEngine#decrypt(byte[], javax.crypto.spec.SecretKeySpec)}.
	 */
	@Test
	public void testDecrypt() {
		fail("Not yet implemented");
	}

}
